const { Document } = require(`flexsearch`);

var fs = require('file-system');

const searchIndexPath = '/home/acka47/git/flexsearch-test/index/'

const index = new Document({
  document: {
    index: [
      {
        field: "name",
        tokenize: "full",
        optimize: true,
        resolution: 9
      }
    ],
    language: "de",
    store: true
  }
});

index.add({
  id: 1,
  name: "Archiv"
});
index.add({
  id: 2,
  name: "Museum"
});
index.add({
  id: 3,
  name: "Öffentliche Bibliothek"
});
index.add({
  id: 4,
  name: "Spezialbibliothek"
});
index.add({
  id: 5,
  name: "Universitätsbibliothek"
});
index.add({
  id: 6,
  name: "Fahrbibliothek"
});

index.export(
   (key, data) => fs.writeFileSync(`${searchIndexPath}${key}.json`, data !== undefined ? data : '')
 )

// var data = index.get([7]);
// console.log(data);
// var result = index.search("wissenschaft", { enrich: true });
// console.log(result, JSON.stringify(result));
